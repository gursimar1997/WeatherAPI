function onEnter(event) {
    if ((event.code === 'Enter' || event.code === 'NumpadEnter')) {
        showWeather();

    }
}

function showWeather() {
    var weather = new XMLHttpRequest();
    var city = document.getElementById('City').value;
    document.getElementById("City").value = " ";
    weather.open("GET", "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&APPID=00a8e2ac52c89f2edb660d21ddb970d8", true);
    weather.send();
    var container = document.getElementById('weather');
    while (container.hasChildNodes()) {
        container.removeChild(container.lastChild);
    }
    weather.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var r = JSON.parse(weather.response);
            var text = document.createTextNode(city + " : " + r.main.temp + " " + "C");
            var ptag = document.createElement("p");
            ptag.appendChild(text);
            var parent = document.getElementById("weather");
            while (parent.hasChildNodes()) {
                parent.removeChild(parent.lastChild);
            }
            parent.appendChild(ptag);
        } else {
            let response = JSON.parse(this.responseText);
            document.getElementById("weather").innerHTML = "<p id=\"result\">"+response.message+"</p>";

        }
    };
}